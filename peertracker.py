#!/usr/bin/env python3

# see https://matix.io/finding-peers-from-a-torrent-file-in-python/

import csv
import sys
import time
import btdht
import socket
import bencode
import hashlib
import datetime
import binascii
import cachetools

def main():
    if not len(sys.argv) == 2:
        exit("Usage: peertracker.py mytorrent.torrent")
    torrent_file = sys.argv[1]
    track_peers(torrent_file)


def track_peers(torrent_file):
    torrent = bencode.decode(open(torrent_file, 'rb').read())
    announcers = torrent['announce-list']

    torrent_id = hashlib.sha1(bencode.bencode(torrent['info'])).hexdigest()
    torrent_id = binascii.a2b_hex(torrent_id)

    dht = btdht.DHT()
    dht.start()
    time.sleep(15)

    out = csv.writer(open(torrent_file + '.csv', 'a'))
    while True:
        now = datetime.datetime.now().isoformat()
        results = dht.get_peers(torrent_id)
        if results:
            for ip, port in results:
                row = [now, ip, port, hostname(ip)]
                out.writerow(row)
        time.sleep(20)


cache = cachetools.TTLCache(maxsize=100000, ttl=360)
def hostname(ip):
    result = None
    if ip in cache:
        result = cache[ip]
    else:
        result = socket.getfqdn(ip)
        cache[ip] = result
    return result


if __name__ == "__main__":
    main()

